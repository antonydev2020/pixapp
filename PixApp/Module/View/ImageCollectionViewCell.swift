//
//  ImageCollectionViewCell.swift
//  PixApp
//
//  Created by Antony Alexander Mylvaganam on 1/27/22.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var photographerImage: UIImageView!
    @IBOutlet private weak var photographerLabel: UILabel!
}
