//
//  CellViewModel.swift
//  PixApp
//
//  Created by Antony Alexander Mylvaganam on 1/27/22.
//

import Foundation

protocol CellConfigurable {
    var pixPhotoURL: String? { get }
    var pixPhotographerName: String? { get }
    
    var pexPhotoURL: String? { get }
    var pexPhotographerName: String? { get }
}

class CellViewModel: CellConfigurable {
//    private let photoModel:
//    init(photo: )
    
    var pixPhotoURL: String?
    
    var pixPhotographerName: String?
    
    var pexPhotoURL: String?
    
    var pexPhotographerName: String?
}
