//
//  ViewController.swift
//  PixApp
//
//  Created by Antony Alexander Mylvaganam on 1/26/22.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var pixSearchBar: UISearchBar! {
        didSet {
            pixSearchBar.delegate = self
        }
    }
    
    @IBOutlet private weak var imageCollectionView: UICollectionView!
    
    var dataSource: [[String: Any]] = [[:]]
    
    private let group = DispatchGroup()
    private let myConCurrentQueue = DispatchQueue(label: "concurrent", attributes: .concurrent)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    private func setUp(searchQuery: String) {
        handshakePixBay(searchQuery)
        handshakePexel(searchQuery)
        group.notify(queue: .main) {
            print(self.dataSource)
            print("All Done")
        }
    }
    
    private func handshakePixBay(_ searchQuery: String) {
        
        let imageTypeParam = Constants.PixBayConstant.Params.imageType + "photo"
        let pageParam = Constants.PixBayConstant.Params.page + "1"
        let perPageParam = Constants.PixBayConstant.Params.perPage + "3"
        
        let finalParams = imageTypeParam + pageParam + perPageParam
//        let finalParams = "&image_type=photo&page=1&per_page=3"
        
        let pixBay = PixBay.urlAndApiKey + "&q=\(searchQuery)" + finalParams
        
        guard let safeURL = URL(string: pixBay) else { return }
        var request = URLRequest(url: safeURL)
        
        NetworkManager.singleton.fetch(request: request) { [weak self] (result: Result<[String: Any], APIError>) in
            guard let self = self else { return }
            self.group.enter()
            switch result {
            case .success(let model):
                self.myConCurrentQueue.async {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                        guard let images = model["hits"] as? [[String: Any]] else { return }
                        self.dataSource.append(contentsOf: images)
                        print("Retrieved Data from PixBay")
                        self.group.leave()
                    }
                }
                
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { _ in }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                self.group.leave()
            }
        }
    }
    
    private func handshakePexel(_ searchQuery: String) {
        
        let pageParam = Constants.PexelsConstant.Params.page + "1"
        let perPageParam = Constants.PexelsConstant.Params.perPage + "3"
        
        let finalParams = pageParam + perPageParam
//        let finalParams = "&page=1&per_page=3"
        
        let pexelURL = Pexels.imageUrl + "?query=\(searchQuery)" + finalParams
        
        guard let safeURL = URL(string: pexelURL) else { return }
        var request = URLRequest(url: safeURL)
        request.allHTTPHeaderFields = Constants.PexelsConstant.headerAPI
        
        group.enter()
        NetworkManager.singleton.fetch(request: request) { [weak self] (result: Result<[String: Any], APIError>) in
            guard let self = self else { return }
            switch result {
            case .success(let model):
                self.myConCurrentQueue.async {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                        guard let images = model["photos"] as? [[String: Any]] else { return }
                        self.dataSource.append(contentsOf: images)
                        print("Retrieved Data from Pexels")
                        self.group.leave()
                    }
                }
                
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { _ in }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                self.group.leave()
            }
        }
    }
}

extension ViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        search()
    }

    func search(isNewSearch: Bool = false) {
        guard let searchText = pixSearchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
              !searchText.isEmpty else { return }
        setUp(searchQuery: searchText)
    }
}
