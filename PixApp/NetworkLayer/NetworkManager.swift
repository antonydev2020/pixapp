//
//  NetworkManager.swift
//  PixApp
//
//  Created by Antony Alexander Mylvaganam on 1/26/22.
//

import Foundation

enum APIError: Error {
    case badURL
    case networkError
    case badResponse
    case noData
    case cannotParse
    case serverError
}

class NetworkManager {
    static let singleton = NetworkManager()
    
    private init() {}
    
    func fetch(request: URLRequest, completionHandler: @escaping (Result<[String: Any], APIError>) -> Void) {
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let completionMain: (Result) -> Void = { result in
                DispatchQueue.main.async {
                    completionHandler(result)
                }
            }
            
            guard error == nil else {
                completionMain(.failure(.networkError))
                return
            }
            
            guard let safeResponse = response as? HTTPURLResponse else {
                completionMain(.failure(.badResponse))
                return
            }
            
            switch safeResponse.statusCode {
            case 200...299:
                guard let safeData = data else {
                    completionMain(.failure(.noData))
                    return
                }
                do {
                    guard let jsonObj = try JSONSerialization.jsonObject(with: safeData, options: .fragmentsAllowed) as? [String: Any] else {
                        completionMain(.failure(.cannotParse))
                        return
                    }
                    completionMain(.success(jsonObj))
                } catch {
                    completionMain(.failure(.cannotParse))
                }
                
            default:
                completionMain(.failure(.serverError))
            }
        }
        task.resume()
    }
}
