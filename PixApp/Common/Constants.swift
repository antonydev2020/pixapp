//
//  Constants.swift
//  PixApp
//
//  Created by Antony Alexander Mylvaganam on 1/26/22.
//

import Foundation

enum PixBay {
    static let urlAndApiKey = Constants.PixBayConstant.baseURL + Constants.PixBayConstant.apiKey
}

enum Pexels {
    static let imageUrl = Constants.PexelsConstant.baseURL + Constants.PexelsConstant.path
}

enum Constants {
    
    enum PixBayConstant {
        static let baseURL = "https://pixabay.com/api/"
        static let apiKey = "?key=25426807-a0574cc86d21570ad8bbda307"
        
        enum Params {
            static let imageType = "&image_type="
            static let page = "&page="
            static let perPage = "&per_page="
        }
    }
    
    enum PexelsConstant {
        static let baseURL = "https://api.pexels.com/"
        static let path = "v1/search"
        static let headerAPI = ["Authorization": "563492ad6f91700001000001773fc32bd9b44625bf2e12279fd3473a"]
        
        enum Params {
            static let page = "&page="
            static let perPage = "&per_page="
        }
    }
}
